import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private restClient: RestClientService) { }
  employeelist = [];

  ngOnInit() {
    this.restClient.getemployee().subscribe(rest => {
      console.log('employee list', rest);
      this.employeelist = rest.data;
    });
  }
}
